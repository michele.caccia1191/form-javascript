function Validation(){

let cognome = document.modulo.cognome.value;
let nome = document.modulo.nome.value;
let matricola = document.modulo.matricola.value;
let regioni = document.modulo.regioni.options[document.modulo.citta.selectedIndex].value;
let email = document.modulo.email.value;
let telefono = document.modulo.telefono.value;
let richieste = document.modulo.richieste.value;

if ((nome == "") || (nome == "undefined")) {
    alert("Il campo Nome è obbligatorio.");
    document.modulo.nome.focus();
    return false;
}
 else if ((cognome == "") || (cognome == "undefined")) {
    alert("Il campo Cognome è obbligatorio.");
    document.modulo.cognome.focus();
    return false;
}
else if ((matricola == "") || (matricola == "isNan(matricola)")) {
    alert("Il campo Matricola è obbligatorio.");
    document.modulo.matricola.focus();
    return false;
}
else if ((regioni == "") || (regioni == "undefined")) {
    alert("Il campo Regioni è obbligatorio.");
    document.modulo.regioni.focus();
    return false;
}
else if ((email == "") || (email == "undefined")) {
    alert("Il campo Email è obbligatorio.");
    document.modulo.email.focus();
    return false;
}
else if ((telefono == "") || (telefono == "isNan(telefono)")) {
    alert("Il campo Telefono è obbligatorio.");
    document.modulo.telefono.focus();
    return false;
}
else if ((richeiste == "") || (richieste == "undefined")) {
    alert("Il campo Richieste è obbligatorio.");
    document.modulo.richieste.focus();
    return false;
}
else {
       
        document.modulo.submit();
        alert('OK');
}

}
